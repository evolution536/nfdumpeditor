# README #

This README would normally document whatever steps are necessary to get your application up and running.

### nfdumpeditor ###

nfdumpeditor is a simple command line program that works on both Windows and Linux. It opens and parses nfdump data files and changes the first_seen and last_seen timestamps for each flow record.

### How do I get set up? ###

In Windows, open the project in Visual Studio (written using Visual Studio 2015) and compile the application in Release mode. Make sure to copy the lzo1.dll library file into the working directory.

On Linux, use autoconf to install the tool. First install the liblzo2-dev package (an example on a Debian based distribution follows).

```
apt-get install liblzo2-dev
```

Then, execute the following commands to build and install nfdumpeditor.

```
./autogen.sh
./configure
make
make install
```
