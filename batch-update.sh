#!/bin/sh
# -----------------------------------------------------------
# Author: Gijs Rijnders, SURFnet
#
# Updates timestamps in all nfdump files in the TARGET_DIR
# directory using the nfdumpeditor program.
#
# -----------------------------------------------------------

TARGET_DIR="/data/nfsen/profiles-data/live/upstream";

# Check whether the /data/nfsen/profiles-data/live/upstream directory exists.
if [ ! -d $TARGET_DIR ]; then
  echo "The nfsen data directory does not exist! Exiting...";
  exit 1;
fi

# Let's recreate the directory structure to place new files in.
for dir in `find $TARGET_DIR -type d -print`; do
        newdir=$(printf "$dir" | sed -e 's/2017/2018/g');
        mkdir $newdir;
done

# For each existing file, update its contents.
for file in `find $TARGET_DIR -name *nfcapd* -type f -print`; do
        outfile=$(echo "$file" | sed -e 's/2017/2018/g');
        /opt/nfdumpeditor/bin/nfdumpeditor -f $file -d 9 -w $outfile;
done
