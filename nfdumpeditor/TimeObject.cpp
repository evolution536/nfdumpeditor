/*
 * Author: Gijs Rijnders
 *
 * This file is part of nfdumpeditor.
 *
 * nfdumpeditor is free software : you can redistribute it and / or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * nfdumpeditor is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with nfdumpeditor.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "TimeObject.h"
#include <cstring>

// Default constructor for the TimeObject class.
TimeObject::TimeObject(const time_t rawTime)
{
	this->rawTime = rawTime;
}

// Default destructor for the TimeObject class.
TimeObject::~TimeObject()
{

}

// =============================================================================================================

// Gets the raw timestamp in long integer format.
const time_t TimeObject::GetRawTime() const
{
	return this->rawTime;
}

// Converts a long timestamp to a human readable date string of the format: <day-month-year hour:minute:second>
const std::string TimeObject::GetDateTimeString() const
{
	// Create a time structure from the raw time in this TimeObject.
	struct tm* dt;
	char buffer[30];
	dt = localtime(&this->rawTime);

	// Convert the time structure to a string and return it.
	strftime(buffer, sizeof(buffer), "%d-%m-%Y %H:%M:%S", dt);
	return std::string(buffer);
}

// Gets a datetime structure from a long timestamp integer.
void TimeObject::GetDateTimeStruct(struct tm* const timeStruct) const
{
	// Create a time structure from the raw time integer in this TimeObject and return it.
	struct tm* _tm = localtime(&this->rawTime);
	memcpy(timeStruct, _tm, sizeof(tm));
}

// =============================================================================================================

// Adds a number of years to the time object. A negative number of years will subtract years from the time object.
TimeObject& TimeObject::AddYears(const int years)
{
	struct tm* _tm = localtime(&this->rawTime);
	_tm->tm_year += years;

	// Does this return the correct time, taking local timezone into account?
	this->rawTime = mktime(_tm);

	// Return self-instance for function chaining.
	return *this;
}
